#!/bin/sh
#There is need to check whether partition exists or not.
DIR="${BASH_SOURCE%/*}"
if [[ ! -d "$DIR" ]]; then DIR="$PWD"; fi
. "$DIR/../../lib/print.sh"
. "$DIR/../../lib/readio.sh"

function wipe_disks {
    print_step "Wipe disks"
    while(true)
    do
        readio "Wanna wipe disks? (yes/no) "
        if [ ${buffer} == "yes" ] ; then
            lsblk
            readio "Choose the disk you want to wipe: /dev/"
            shred -v /dev/${buffer}
        elif [ ${buffer} == "no" ] ; then
            println ""
            break;
        else
            print_warning "Please type yes or no"
        fi
    done
}

function partition_disks {
    print_step "Partition disks"
    while(true)
    do
        readio "Wanna partition disks? (yes/no) "
        if [ ${buffer} == "yes" ] ; then
            lsblk
            readio "Choose the disk you want to partition: /dev/"
            cgdisk /dev/${buffer}
        elif [ ${buffer} == "no" ] ; then
            println ""
            break;
        else
            print_warning "Please type yes or no"
        fi
    done
}

function specify_boot_partition {
    print_step "Which partition is boot partition?"
    lsblk
    readio " /dev/"
    mkfs.fat -F32 /dev/${buffer}
}

function specify_root_partitions {
    print_step "Specify Solaris Root partitions"
    lsblk
    readio "/dev/"
    solaris_root+=(${buffer})

    while(true)
    do
    	readio "Any other partitions for Solaris Root? (yes/no) "
    	if [ ${buffer} == "yes" ] ; then
            lsblk
    		readio "/dev/"
    		solaris_root+=(${buffer})
    	elif [ ${buffer} == "no" ] ; then
            println ""
    		break;
    	else
    		print_warning "Please type yes or no"
    	fi
    done
}

function update_mirrorlist {
    wget -O /etc/pacman.d/mirrorlist https://www.archlinux.org/mirrorlist/all/
    sed -i '/Taiwan/,+4s/^#//g' /etc/pacman.d/mirrorlist
}

function install_and_load_zfs_packages {
    print_step "Installing ZFS packages."

    print "Adding Repo..."
    echo -e "\n[archzfs]\nSigLevel = Required DatabaseOptional TrustedOnly\nServer = "\
            "http://demizerone.com/\$repo/x86_64" >> /etc/pacman.conf

    println "Importing and signing key..."
    pacman-key -r 0EE7A126
    pacman-key --lsign-key 0EE7A126
    pacman -Syy
    pacman -S zfs-linux-git
    println "Load zfs modules..."
    #depmod -ae
    modprobe zfs
}

function create_zpool_and_mount_mnt {
    print_step "Create zpool automatically."
    ls -l /dev/disk/by-partuuid/
    for i in `seq 1 ${#solaris_root[@]}`;
    do
        concatence+=" /dev/disk/by-partuuid/"
        concatence+=$(blkid -s PARTUUID -o value /dev/${solaris_root[i-1]})
    done
    zpool create -f zroot ${concatence}
    zfs set mountpoint=/ zroot
    zfs create zroot/root -o mountpoint=/root
    zpool set bootfs=zroot zroot
    zpool export zroot
    zpool import -d /dev/disk/by-partuuid/ -R /mnt zroot
    mount
    df -h
    println "For debugging"
}

#Execute above functions sequentially
wipe_disks
partition_disks
specify_boot_partition
specify_root_partitions
update_mirrorlist
install_and_load_zfs_packages
create_zpool_and_mount_mnt
