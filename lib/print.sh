function print_error {
    echo -e "\033[31m###$1###\033[0m"
}

function println_error {
    echo -e "\033[31m###$1###\n\033[0m"
}

function print_step {
    echo -e "\033[33m###$1###\033[0m"
}

function println_step {
    echo -e "\033[33m###$1###\n\033[0m"
}

function print_warning {
    echo -e "\033[33m$1\033[0m"
}

function println_warning {
    echo -e "\033[33m$1\n\033[0m"
}

function print {
    echo $1
}

function println {
    echo -e "$1\n"
}
